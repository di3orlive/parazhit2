import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {ApiService} from './shared';
import {routing} from './app.routing';

import {AppComponent} from './app.component';
import {PrzHomeComponent} from './home/home.component';
import {PrzMenuComponent} from './menu/menu.component';
import {PrzPlayerComponent} from "./player/player.component";
import {PrzContactsComponent} from "./contacts/contacts.component";
import {PrzListenComponent} from "./listen/listen.component";



@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing
  ],
  declarations: [
    AppComponent,
    PrzHomeComponent,
    PrzMenuComponent,
    PrzPlayerComponent,
    PrzContactsComponent,
    PrzListenComponent
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
