import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'demoDrop-page',
  templateUrl: './demo.drop.component.html',
  styleUrls: ['./demo.drop.component.scss']
})
export class PrzDemoDropComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {
    console.log('Hello demoDrop');
  }

}
