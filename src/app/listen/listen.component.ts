import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'listen-page',
  templateUrl: './listen.component.html',
  styleUrls: ['./listen.component.scss']
})
export class PrzListenComponent implements OnInit {

  constructor() {
    
  }

  ngOnInit() {
    console.log('Hello listen');
  }

}
