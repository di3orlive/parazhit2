import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'control-page',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class PrzControlComponent implements OnInit {

  constructor() {
    
  }

  ngOnInit() {
    console.log('Hello control');
  }

}
