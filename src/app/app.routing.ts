import {RouterModule, Routes} from '@angular/router';

import {PrzHomeComponent} from './home/home.component';
import {PrzContactsComponent} from "./contacts/contacts.component";
import {PrzListenComponent} from "./listen/listen.component";
import {PrzShowsComponent} from "./shows/shows.component";
import {PrzResidentComponent} from "./resident/resident.component";
import {PrzResidentTTComponent} from "./resident.tt/resident.tt.component";
import {PrzEventsComponent} from "./events/events.component";
import {PrzNewsComponent} from "./news/news.component";
import {PrzNewsItemComponent} from "./news.item/news.item.component";
import {PrzShopComponent} from "./shop/shop.component";
import {PrzShopItemComponent} from "./shop.item/shop.item.component";
import {PrzApplicationsComponent} from "./applications/applications.component";
import {PrzDemoDropComponent} from "./demo.drop/demo.drop.component";
import {PrzPrivacyComponent} from "./privacy/privacy.component";
import {PrzJobsComponent} from "./jobs/jobs.component";
import {PrzJobItemComponent} from "./job.item/job.item.component";
import {PrzControlComponent} from "./control/control.component";
import {PrzCartComponent} from "./cart/cart.component";
import {PrzCheckoutComponent} from "./checkout/checkout.component";
import {PrzUpdateTrackComponent} from "./update.track/update.track.component";
import {PrzUpdateTrack2Component} from "./update.track2/update.track2.component";
import {PrzUpdateTrack3Component} from "./update.track3/update.track3.component";


const routes: Routes = [
  { path: '', redirectTo: 'home', terminal: true},
  { path: 'home', component: PrzHomeComponent},
  { path: 'contacts', component: PrzContactsComponent},
  { path: 'listen', component: PrzListenComponent},
  { path: 'shows', component: PrzShowsComponent},
  { path: 'resident', component: PrzResidentComponent},
  { path: 'resident-timetable', component: PrzResidentTTComponent},
  { path: 'events', component: PrzEventsComponent},
  { path: 'news', component: PrzNewsComponent},
  { path: 'news-item', component: PrzNewsItemComponent},
  { path: 'shop', component: PrzShopComponent},
  { path: 'shop-item', component: PrzShopItemComponent},
  { path: 'applications', component: PrzApplicationsComponent},
  { path: 'demo-drop', component: PrzDemoDropComponent},
  { path: 'privacy', component: PrzPrivacyComponent},
  { path: 'jobs', component: PrzJobsComponent},
  { path: 'job-item', component: PrzJobItemComponent},
  { path: 'control', component: PrzControlComponent},
  { path: 'cart', component: PrzCartComponent},
  { path: 'checkout', component: PrzCheckoutComponent},
  { path: 'update-track', component: PrzUpdateTrackComponent},
  { path: 'update-track2', component: PrzUpdateTrack2Component},
  { path: 'update-track3', component: PrzUpdateTrack3Component}
];

export const routing = RouterModule.forRoot(routes);
